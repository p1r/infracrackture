import sys
import colorama
from colorama import Fore, Style
import pyfiglet
from pyfiglet import Figlet
import argparse
import subprocess

# banner-contenders=['univers','sub-zero', 'swampland', 'roman', 'georgia11', 'eca_____', 'clb8x8']
clear_screen='\033[2J'
purple="\033[94m {}\033[00m"
bold = '\033[1m'
custom_fig = Figlet(font='twin_cob')
custom_fig2 = Figlet(font='contessa')
custom_fig3 = Figlet(font='swampland')
custom_fig4 = Figlet(font='wow')
hello="OOOOH YEAH"

print(clear_screen + Fore.GREEN + Style.BRIGHT + '=' * 72 + '\n')
p1r = custom_fig2.renderText('.##Created by p1r##.')
hej = custom_fig3.renderText('tst')

print(Fore.BLUE + custom_fig2.renderText('#' * 12))
print(Fore.GREEN + Style.BRIGHT + custom_fig.renderText('infra-----crackture'))
print(Fore.BLUE + p1r)
print(Fore.GREEN + Style.BRIGHT + '=' * 72 + '\n')



parser = argparse.ArgumentParser(usage="infracrackture -t <threshold> -m <email_address>", add_help=False)
required = parser.add_argument_group('required arguments')
optional = parser.add_argument_group('optional arguments')
optional.add_argument("-t", "--threshold", action="store", metavar='', help="Threshold in USD"   )
optional.add_argument("-m", '--mail-address', metavar='', help="E-mail to be notified when threshold exceeded")
optional.add_argument('-h', '--help', help="Print help and exit", action='store_true')

args = parser.parse_args()

parser.print_help()
# twin_cob x
# swampland
# space_op
# p_s_h_m
# npn_____ x
# home_pak
# fbr2____
# ebbs_2__
# eca_____

# infracrackture med twin_cob är vinnarn

# men annars crack-attack med defleppard är jäävligt frestande.

# Created by p1r
# threepoint
# straight
# mini
# slscript
# digital
# contessa
# bigfig