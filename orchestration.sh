#!/bin/bash

# aws configure --profile crack-user set default.region $1

# Okay so there's two different AMIs; standard ubuntu and ubuntu for deep learning compare prices
# crack-attack -c --ami standard-ubuntu --notify-on-cost [] --max-cost-threshold --mail example@example.com

# Create an IAM-User (crack-user lol)
# add SNS-stuff

# Resources:
# https://ben.the-collective.net/2019/07/10/hashcat-in-aws-ec2/
# 
# https://medium.com/@iraklis/running-hashcat-v4-0-0-in-amazons-aws-new-p3-16xlarge-instance-e8fab4541e9b

# REGION="$1"
STACKNAME="infracrackture-ec2-sg-stack"
IPWHITELIST="$(curl ipinfo.io -s | jq -r .ip)/32"
KEYNAME="infracrackture-key"
EMAILADDRES=$1
ALARMTHRESHOLD=$2
AMI=ami-0dba2cb6798deb6d8 #"$(aws ec2 describe-images --owners 099720109477 --filters 'Name=name,Values=ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-????????' 'Name=state,Values=available' --output json | jq -r '.Images | sort_by(.CreationDate) | last(.[]).ImageId')"

tearDownInfracrackture(){
    echo "Tearing down infracrackture"
    # pid=$(ps aux | grep -i "ssh -N -C -D 1080 -l ubuntu" | head -n1 | awk '{print $2}')
    # kill -9 $pid 2> /dev/null
    # wait $pid 2> /dev/null
    aws cloudformation delete-stack --stack-name --profile crack-user $STACKNAME
    aws ec2 delete-key-pair --profile crack-user --key-name $KEYNAME
    rm -f ~/.ssh/$KEYNAME*
    exit 0
}

keyCreation(){
    echo "Creating key pair"
    ssh-keygen -q -t rsa -b 2048 -f ~/.ssh/$KEYNAME -N "" > /dev/null
    echo "Exporting public key"
    aws ec2 import-key-pair --profile crack-user --key-name "$KEYNAME" --public-key-material file://~/.ssh/$KEYNAME.pub > /dev/null
    mv -f ~/.ssh/$KEYNAME ~/.ssh/$KEYNAME.pem # AWS requires this naming format
    chmod 400 ~/.ssh/$KEYNAME.pem # and these permissions
}

keyCreation

echo "Creating cloudformation stack"
aws cloudformation create-stack --profile crack-user --parameters \
ParameterKey=Ip,ParameterValue=$IPWHITELIST \
ParameterKey=KeyName,ParameterValue=$KEYNAME \
ParameterKey=Ami,ParameterValue=$AMI \
ParameterKey=EmailAddress,ParameterValue=$EMAILADDRESS \
ParameterKey=AlarmThreshold,ParameterValue="500" \
--stack-name $STACKNAME \
--template-body file://cfn-ec2-sg-cw.yml > /dev/null \
|| tearDownInfracrackture

while [ $(aws cloudformation describe-stacks --stack-name $STACKNAME | jq -r .Stacks[0].StackStatus) = 'CREATE_IN_PROGRESS' ]
do
    echo "Creating infracrackture..."
    sleep 5
done

if [ $(aws cloudformation describe-stacks --stack-name $STACKNAME | jq -r .Stacks[0].StackStatus)  = 'CREATE_COMPLETE' ]
then
    echo "Create complete"
else 
    echo "Something went wrong."
    #tearDownInfracrackture
    exit -1
fi

# god this is ugly
REMOTE_IP=$(aws ec2 describe-instances --query 'Reservations[*].Instances[*].[PublicIpAddress]' --filters Name=instance-state-name,Values=running \
 | tr -d '\n\"[:space:][\]')

printf "Configuring infracrackture\nInitializing setup\n"
sleep 13 # If Connection refused then increase this sleep time 
# scp -i ~/.ssh/"$KEYNAME.pem" sshd_config ubuntu@$REMOTE_IP:/etc/ssh
# ssh ubuntu@$REMOTE_IP -i ~/.ssh/$KEYNAME.pem -o StrictHostKeyChecking=no \
# sudo service sshd restart
echo "letsgo"
# printf 'Server is all setup\nTunneling through port 1080\n'
# ssh -C -l ubuntu -i ~/.ssh/$KEYNAME.pem $REMOTE_IP
